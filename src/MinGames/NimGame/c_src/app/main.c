/*******************************************  主函数  ********************************************/
/*																								 */
/*	主函数:																						 */
/*		1、	清屏																				 */
/*		2、	获取随机数																			 */
/*		3、	开始游戏页面显示																	 */
/*		4、	游戏结果显示																		 */
/*		5、	进入下一局游戏																		 */
/*						 																		 */
/*************************************************************************************************/
/*						 																		 */
/*	更新记录：					 																 */
/*		1、新增游戏过程记录功能		   —— 于2019年9月27日				 						 */
/*		2、新增游戏限时功能			   —— 于2019年9月29日				 						 */
/*		3、新增游戏选择页面退出返回    —— 于2019年9月29日										 */
/*		4、新增写入文件游戏时间		   —— 于2019年9月29日										 */
/*						 																		 */
/*************************************************************************************************/
/****************************************  头文件包含  ******************************************/
#include "qizi.h"
/*****************************************  参数定义  *******************************************/
uint Ahear, Bhear, Chear;		// 定义三堆#include "qizi.h"
uint Name[3];	// static 区别 extern 定义在头文件中extern会报错“符号被多重定义” static不会
uint * Namehear = Name;
uint getout = 0;			// 取那一堆
uint number = 0;			// 取几个
uint * Namenumber = &number;
uint flag;	// 游戏标志位 11、12、13、2
uint choose_people = 0;		// 选择游戏模式
uint choose_difficulty = 0;	// 选择游戏难度
// 定义时间
time_t starttime = 0;
time_t endtime = 0;
//pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;		// 锁线程时用的 锁初始化

/*****************************************   主函数   ******************************************/
int main()
{
	system("color e0");		// 黄色背景好看

	while (1)
	{
		// 游戏部分
	// 1、清屏
		system("cls");
	// 2、获取随机数
		srand((unsigned)time(NULL));
		Namehear[1] = Ahear = 10 + rand() % MAX;
		Namehear[2] = Bhear = 10 + rand() % MAX;
		Namehear[3] = Chear = 10 + rand() % MAX;
		/*  用来除BUG时进行赋值 */
//		Namehear[1] = Ahear = 1;
//		Namehear[2] = Bhear = 9;
//		Namehear[3] = Chear = 8;
		/**********************************   开始  *******************************************/
	// 3、开始游戏页面显示
			game(choose());						// 正式进入游戏，其中先进入choose()再进入game()
	// 4、游戏结果显示
	/* 结束 */
	}
}





// 乱七八糟的多线程部分
#ifdef DEBUG
// 线程1
   /*********************************************************************
	*	多线程的同步与互斥
	*	方式一：锁
	*	1、在主线程中初始化锁为解锁状态
	* 			pthread_mutex_t mutex;
	*			pthread_mutex_init(&mutex, NULL);
	* 	2、在编译时初始化锁为解锁状态
	* 		锁初始化
	*			pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	* 	3、访问对象时的加锁操作与解锁操作
	* 		加锁		pthread_mutex_lock(&mutex)
	* 		释放锁		pthread_mutex_unlock(&mutex)
	********************************************************************/
	/*********************************   初始化  ******************************************/
	// 多线程
int tmp1;
void * retval;
pthread_t thread1;
char *message1 = "thread1";
int ret_thrd1;
/*
 * 创建线程：pthread_create(&thread1, NULL, (void *)&print_message_function, (void *)message1);
 *     参数：
 *			1. 一个线程变量名，被创建线程的标识		- pthread_t thrd1;
 *			2. 线程的属性指针，缺省为NULL即可		- pthread_attr_t attr;
 *			3. 被创建线程的程序代码					- void thread_function(void argument);
 *			4. 程序代码的参数						- char *some_argument;
 */
ret_thrd1 = pthread_create(&thread1, NULL, (void *)&print_message_function, (void *)message1);

// 线程创建成功，返回0,失败返回失败号
if (ret_thrd1 != 0) {
	printf("线程1创建失败\n");

}
else {
	printf("线程1创建成功\n");

}
/*
 * 线程等待：tmp1 = pthread_join(thread1, &retval);
 * 使用目的：pthread_create调用成功以后，新线程和老线程谁先执行，谁后执行用户是不知道的，这一块取决与操作系统对线程的调度，如果我们需要等待指定线程结束，
 *		     需要使用pthread_join函数，这个函数实际上类似与多进程编程中的waitpid。 举个例子，以下假设 A 线程调用 pthread_join 试图去操作B线程，该函数将A线程阻塞，
 *           直到B线程退出，当B线程退出以后，A线程会收集B线程的返回码。 该函数包含两个参数：
 *     参数：
 *			1. th是要等待结束的线程的标识									pthread_t th
 *			2. 指针thread_return指向的位置存放的是终止线程的返回状态。		void **thread_return
 */
tmp1 = pthread_join(thread1, &retval);		//同样，pthread_join的返回值成功为0
printf("thread1 return value(retval) is %d\n", (int)retval);
printf("thread1 return value(tmp) is %d\n", tmp1);
if (tmp1 != 0)
{
	printf("cannot join with thread1\n");
		}
printf("thread1 end\n");
system("pause");
#endif // DEBUG
#ifdef DEBUG
unsigned char print_message_function(void *ptr)
{

	long i, tmp;
	for (i = 0; i <= 100000; i++) {
		/*加锁*/
		if (pthread_mutex_lock(&mutex) != 0) {
			perror("pthread_mutex_lock");
			exit(EXIT_FAILURE);
		}
		/************************ 线性主程序部分 ***********************/

		// 定义时间
/*		time_t timep;	// 定义一个时间类型的变量
		struct tm *p;	// 定义一个时间类型的指针变量

		// 获取初始系统时间
		time(&timep);
		p = gmtime(&timep);		// 获取当前时间
		unsigned char gametimesec = p->tm_sec;
		unsigned char gametimemim = p->tm_min;
		*/
//		printf("%d : %d", gametimesec, gametimemim);
		unsigned char i;
		for (i = 0; i < 6; i++)
		{
//			sleep(10000);	// 10s
//			printf("%d : %d : 还剩下 %d 秒", gametimesec, gametimemim, (60 - i * 10));
			printf("还剩下 %d 秒", (60 - i * 10));
		}
//		sleep(10000);	// 10s
		return 0;		// 时间到


		/****************************************************************/
		/*解锁*/
		if (pthread_mutex_unlock(&mutex) != 0) {
			perror("pthread_mutex_unlock");
			exit(EXIT_FAILURE);
		}
	}
}
#endif // DEBUG

/* 测试代码 */
/*
	display();
	printf("***************************** 测试代码 ******************************\n");
	printf("Ahear, Bhear, Chear = %d,%d,%d\n", Ahear, Bhear, Chear);
	printf("Namehear[1], Namehear[2], Namehear[3] = %d,%d,%d\n", Namehear[1], Namehear[2], Namehear[3]);
	printf("flag = %d\n", flag);
	printf("***************************** 测试代码 ******************************\n");
	system("pause");
*/