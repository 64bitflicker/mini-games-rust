/****************************************  交互操作函数  *****************************************/
/*																								 */
/*	函数实现:																					 */
/*		1、交互界面定义																			 */
/*		2、对输入进行校验																		 */
/*		3、判断胜负函数																			 */
/*		4、	异常处理																			 */
/*						 																		 */
/*************************************************************************************************/

/****************************************  头文件包含  ******************************************/
#include "qizi.h"

/****************************************   函数原型   *****************************************/
// 交互界面1
void  interaction()
{
	/* 测试完成 稳得
	printf("***************************** 测试代码 ******************************\n");
	printf("Ahear, Bhear, Chear = %d,%d,%d\n", Ahear, Bhear, Chear);
	printf("Namehear[1], Namehear[2], Namehear[3] = %d,%d,%d\n", Namehear[1], Namehear[2], Namehear[3]);
	printf("flag = %d\n", flag);
	printf("***************************** 测试代码 ******************************\n");
	system("pause");
	*/
	{
		printf("NPC:你要取那一堆?（输入1 或 2 或 3 ）\n");
		/*
		 *	初始化锁 
		pthread_mutex_t mutex;
		pthread_mutex_init(&mutex, NULL);
		print_message_function(NULL);
		 */
		scanf("%d", &getout);
		getchar();
	}
	switch (getout)
	{
	case 1: check_number(getout, number); break;
	case 2: check_number(getout, number); break;
	case 3: check_number(getout, number); break;
	default: {
		// 错误	
		getout = 0;
		error();
		display();
		interaction();
	}; break;
	}
	//输入的校验
	writefile_num(11);			// 写入文件进行记录
}

// 交互界面2
void  interaction_1v1(uchar i)
{
	if(i == 1){
		printf("玩家1:你要取那一堆?（输入1 或 2 或 3 ）\n");
	}
	else 
	{
		printf("玩家2:你要取那一堆?（输入1 或 2 或 3 ）\n");
	}
	scanf("%d", &getout);
	getchar();
	switch (getout)
	{
	case 1: check_number_1v1(getout, number, i); break;
	case 2: check_number_1v1(getout, number, i); break;
	case 3: check_number_1v1(getout, number, i); break;
	default: {
		// 错误	
		getout = 0;
		error();
		display();
		interaction_1v1(i);
	}; break;
	}
	//输入的校验
}

// 交互界面副函数1（对每次取棋子数进行校验）
uchar check_number(uint getout, uint number)
{
	printf("NPC:你要取几个棋子?（最少1个最多为全部 ）\n");
	scanf("%d", &number);
	getchar();
	if (IF_GameTime(30, 1))
	{
		// 游戏结束
		printf("你输了！\n游戏结束！\n");
		system("pause");
		choose();
	}
	if ((getout == 1) && (number > 0) && (number <= Ahear))
	{
		Namehear[1] = Ahear -= number;

	}
	else if ((getout == 2) && (number > 0) && (number <= Bhear))
	{
		Namehear[2] = Bhear -= number;
	}
	else if ((getout == 3) && (number > 0) && (number <= Chear))
	{
		Namehear[3] = Chear -= number;
	}
	else
	{
		// 错误
		number = 0;
		error();
		display();
		interaction(Namehear[3]);
	}
}

// 交互界面副函数2（对每次取棋子数进行校验）
uchar check_number_1v1(uint getout, uint number, uchar i)
{
	if (i == 1) {
		printf("玩家1:你要取几个棋子?（最少1个最多为全部 ）\n");
	}
	else
	{
		printf("玩家2:你要取几个棋子?（最少1个最多为全部 ）\n");
	}
	scanf("%d", &number);
	getchar();
	if (IF_GameTime(30, 1))
	{
		// 游戏结束
		printf("游戏结束！\n");
		system("pause");
		choose();

	}
	if ((getout == 1) && (number > 0) && (number <= Ahear))
	{
		Namehear[1] = Ahear -= number;
	}
	else if ((getout == 2) && (number > 0) && (number <= Bhear))
	{
		Namehear[2] = Bhear -= number;
	}
	else if ((getout == 3) && (number > 0) && (number <= Chear))
	{
		Namehear[3] = Chear -= number;
	}
	else
	{
		// 错误
		number = 0;
		error();
		display();
		interaction(Namehear[3]);
	}
}

// 判断胜负函数(You Win!)
uint How_Win(){
	int Sum;
	Sum = Namehear[1] + Namehear[2] + Namehear[3];
	//Sum = Namehear[1] + Namehear[2] + Namehear[3];
	if (Sum == 0) {
		printf("\nYou Win! \nGame Over!\n\n");
		system("pause");
		return 1;
	}
	else {
		printf("\n游戏继续!\n\n");
		system("pause");
		return 0;
	}
}
// 判断胜负函数(You Lose!)
uint How_Lose() {
	int Sum;
	Sum = Namehear[1] + Namehear[2] + Namehear[3];
	//Sum = Namehear[1] + Namehear[2] + Namehear[3];
	if (Sum == 0) {
		display();
		printf("\nYou Lose! \nGame Over!\n\n");
		system("pause");
		return 1;
	}
	else {
		printf("\n游戏继续!\n\n");
		system("pause");
		return 0;
	}
}
// 错误！！警告
void error(void)
{
	printf("\n\terror!\n");
	printf("\n\t你是沙雕吗，这都能输错!\n");
	system("pause");
}

// 计时函数
// 游戏时间记录函数
// 参数
//	  1、定时数
//    2、模式选择 0 -> 记录开始时间 ；  1 -> 记录结束时间并判断 
uchar IF_GameTime(uchar times, uchar choose)
{

	if (choose == 0) // 计starttime时
	{
		// 1.读取时间保存在starttime中
		starttime = time(NULL);	// 读取1970到当前系统的时间的秒数
		return 0;
	}
	else if(choose == 12)		// 进入机器人结束计时模式
	{
		// 1.读取时间保存在endtime中
		endtime = time(NULL);
		printf("机器人用时 %d s\n", endtime - starttime);
		return 1; // 用来返回结束信息
	}
	else		// 进入结束计时模式
	{

		// 1.读取时间保存在endtime中
		endtime = time(NULL);
		// 2.
				// 我们给玩家的游戏时间为times
		// 3.计算总用时,并判断是否超时
			// 3.1.超时则进行用时显示，返回，并清空输入
			// 3.2.未超时则进行用时显示，不清空输入
		if ((endtime - starttime) > times)
		{
			// 那么玩家超时了
				// 将输入清空
			getout = 0;			// 那一堆清零
			number = 0;			// 取几个清零
			printf("玩家您用时 %d s\n", endtime - starttime);
			printf("您超出了规定的时间！！\n");
			return 1; // 用来返回结束信息
		}
		else
		{
			// 玩家没有超时
			printf("玩家您用时 %d s\n", endtime - starttime);
			return 0; // 用来返回结束信息
		}
	}
	// 讲用时写入文件
}


// 文件写入
// 参数 等于1 则写入新的一局标题
//      等于0  则写入玩家用时
//		其他 写入棋子数
void writefile_num(unsigned char choose)
{
	FILE * out;
	out = fopen("test.txt", "a+");	// 没有将创建文件
	if (out == NULL) {				// 读文件
		exit(EXIT_FAILURE);
	}
	// 写文件
	int i;
	char tmp[20];
	char tme[20];
	if (choose == 1)
	{
		fprintf(out, "\n新的一局开始 :\n", i);

	}
	else if (choose == 12)
	{
		fprintf(out, "当前剩余棋子数 :\t", i);
		for (i = 1; i < 4; i++) {
			// 这里将整形数转为字符串
			// 第一种
			sprintf(tmp, "第%d堆 : %5d\t", i, Namehear[i]);
			// 第二种
			//	itoa(Namehear[i], tmp, 10);		在这里不方便
			fprintf(out, tmp, i);
		}
		sprintf(tme, "机器人用时 : %d s\n", endtime - starttime);
		fprintf(out, tme, i);

	}
	else
	{
		fprintf(out, "当前剩余棋子数 :\t", i);
		for (i = 1; i < 4; i++) {
			// 这里将整形数转为字符串
			// 第一种
			sprintf(tmp, "第%d堆 : %5d\t", i, Namehear[i]);
			// 第二种
			//	itoa(Namehear[i], tmp, 10);		在这里不方便
			fprintf(out, tmp, i);
		}
		sprintf(tme, "玩家用时 : %d s\n", endtime - starttime);
		fprintf(out, tme, i);
	}
	fclose(out);
	return;
}
