/****************************************  显示界面函数  *****************************************/
/*																								 */
/*	函数实现:																					 */
/*		1、显示开始界面进行——游戏模式选择														 */
/*		2、游戏画面定义																			 */
/*		3、交互界面定义																			 */
/*		4、对输入进行校验处理异常																 */
/*						 																		 */
/*************************************************************************************************/

/****************************************  头文件包含  ******************************************/
#include "qizi.h"

/*****************************************  参数定义  *******************************************/


/****************************************   函数原型   *****************************************/
void display() {
	// 1、清屏
	// 保证每次显示前都更新了数据
	//Ahear = Namehear[1];
	//Bhear = Namehear[2];
	//Chear = Namehear[3];
	system("cls");
	// 2、显示
	printf("\t\t\n\t\t三堆棋子：\n\n");
	printf("\t\t**************************************************\n\n");
//	printf("|       |        |        |\n\n");
	printf("\t\t||     第1堆\t||     第2堆\t||     第3堆\t||\n\n");
//	printf("|       |        |        |\n\n");
	printf("\t\t*************************************************\n\n");
//	printf("|       |        |        |\n\n");
//	printf("|  %d   |   %d   |   %d   |\n\n", Ahear, Bhear, Chear);
	printf("\t\t||\t%d\t||\t%d\t||\t%d\t||\n\n", Namehear[1], Namehear[2], Namehear[3]);
//	printf("|       |        |        |\n\n");
	printf("\t\t**************************************************\n\n");
}

// 游戏开始选择界面
uchar choose(void)
{
	// 1、清屏
	system("cls");
	// 2、显示
	printf("选择游戏模式： \n\t1 : 单人游戏/(隐藏模式）\n\t2 : 双人游戏\n\t0 : 退出\n\n");
	printf("请输入你的选择（1/2） ：\n");
	scanf("%d", &choose_people);
	getchar();
	//输入的校验
	switch (choose_people)
	{
		case 0:
			exit(0);
		case 1:
			return check_gedree(choose_people, choose_difficulty);
		case 2: 
			return 2; 
		default: {
			// 错误	
			error();
			choose_people = 0;
			choose_difficulty = 0;	
			choose();
			break;
		}
	}
}

// 游戏开始选择界面副函数（对游戏模式选择进行校验）
uchar check_gedree(uint choose_people, uint choose_difficulty)
{
	printf("选择游戏难度： \n\t1 : 简单 \n\t2 : 正常\n\t3 : 地狱\n\t0 : 返回\n\n");
	printf("请输入你的选择（1/2/3） ：\n");
	scanf("%d", &choose_difficulty);
	getchar();
	if ((choose_people == 1) && (choose_difficulty == 1))
	{
		// 单人简单	
		flag = 11;
	}
	else if ((choose_people == 1) && (choose_difficulty == 2))
	{
		// 单人普通	
		flag = 12;
	}
	else if ((choose_people == 1) && (choose_difficulty == 3))
	{
		// 单人地狱
		flag = 13;
	}
	else if (choose_difficulty == 0)
	{
		choose();
	}
	else {
		// 错误
		choose_people = 0;
		error();
		choose();
	}
	return flag;
}

// 游戏模式确定函数
void game(uchar flag)
{
	/*  测试之前的代码 均可靠
	printf("***************************** 测试代码 ******************************\n");
	printf("Ahear, Bhear, Chear = %d,%d,%d\n", Ahear, Bhear, Chear);
	printf("Namehear[1], Namehear[2], Namehear[3] = %d,%d,%d\n", Namehear[1], Namehear[2], Namehear[3]);
	printf("flag = %d\n", flag);
	printf("***************************** 测试代码 ******************************\n");
	system("pause");	
	*/
	// 游戏标志位 11、12、13、21、22、23
	switch (flag)
	{			
		case 11 :// 单人简单
		{
			writefile_num(1);
			while (1) 
			{	
				IF_GameTime(30, 0);		// 记录开始时间
				display();						
				interaction();			// 进行交互  玩家
				if (How_Win())
				{						// 判断是否结束
					return 0;
				}
				display();
				robot();				// 机器操作
				if (How_Lose())
				{	
					// 判断是否结束
					return 0;
				}
			};
			break;
		}
		case 12 :// 单人普通
		{
			writefile_num(1);
			printf("\t\t 说明：本模式为计时模式（30秒），请玩家在30秒内完成取棋子操作，否自将失败！\n");
			printf("\t\t 一起来快乐的玩耍吧！\n");
			system("pause");
			while (1)
			{
				IF_GameTime(30, 0);		// 记录开始时间
				display();
				interaction();			// 进行交互  玩家
				if (How_Win())
				{						// 判断是否结束
					return 0;
				}
				display();
				IF_GameTime(30, 0);		// 记录开始时间
				robot();				// 机器操作
				if (How_Lose())
				{
					// 判断是否结束
					return 0;
				}
			};
			break;
		}
		case 13 :// 单人地狱(隐藏模式 3P模式)
		{
			writefile_num(1);
			printf("\t\t 说明：欢迎来到隐藏3P模式，本模式为 “你” 与 “两台机器” 一起玩耍！\n");
			printf("\t\t 一起来快乐的3P吧！\n");
			system("pause");
			while (1)
			{
				display();
				robot();	
				if (How_Lose())
				{
					// 判断是否结束
					return 0;
				}
				display();
				interaction();			// 进行交互  玩家
				if (How_Win())
				{						// 判断是否结束
					return 0;
				}
				display();
				robot();				// 机器操作
				if (How_Lose())
				{
					// 判断是否结束
					return 0;
				}
			};
			break;
		}
		case  2 :// 双人
		writefile_num(1);
		{
			display();
			while (1)
			{
				display();
				interaction_1v1(1);			// 进行交互
				if (How_Win())
				{							// 判断是否结束
					break;
				}
				display();
				interaction_1v1(0);			// 进行交互
				if (How_Win())
				{							// 判断是否结束
					break;
				}
			}
			break;
		}
		default :
		{
			return 0;
		}
	}
	printf("游戏结束！\n");
}




