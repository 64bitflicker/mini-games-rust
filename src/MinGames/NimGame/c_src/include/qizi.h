/***************************************  头文件内容定义  ****************************************/
/*																								 */
/*	制作信息：（版权所有）																		 */
/*		1、制作组：6 # 214宿舍																     */
/*		2、算法研究 ：任心刚、潘杰、罗俊清、刘斌												 */
/*		3、代码编写：任心刚																		 */
/*																								 */
/*************************************************************************************************/
/*																								 */
/*	定义内容:																					 */
/*		1、头文件包含																			 */
/*		2、对输入进行校验																		 */
/*		3、判断胜负函数																			 */
/*		4、	异常处理																			 */
/*						 																		 */
/*************************************************************************************************/
#ifndef __QIZI_H__
#define __QIZI_H__

/****************************************  头文件包含  *****************************************/
#include <stdio.h>
//#include <windows.h>
#include <stdlib.h>
//#include <time.h>
#include <ctype.h>
//#include <pthread.h>	//  多线程库

/*****************************************  类型定义  ******************************************/
typedef unsigned char uchar;
typedef unsigned int uint;	

/*****************************************  参数定义  *******************************************/

// 最大数量为MAX=100个
#define MAX 101
// 全局变量声明
extern uint Ahear, Bhear, Chear;		// 定义三堆#include "qizi.h"
extern uint Name[];	// static 区别 extern 定义在头文件中extern会报错“符号被多重定义” static不会
extern uint * Namehear;
extern uint getout;			// 取那一堆
extern uint number;			// 取几个
extern uint * Namenumber;
extern uint flag;	// 游戏标志位 11、12、13、2
extern uint choose_people;		// 选择游戏模式
extern uint choose_difficulty;	// 选择游戏难度
extern time_t starttime;
extern time_t endtime;

// 显示棋子数游戏界面函数
/******************************************  函数声明 *******************************************/

// 显示界面函数声明
	// 显示棋子数游戏界面函数
void display();
	// 游戏开始选择界面
uchar choose(void);
	// 游戏开始选择界面副函数（对游戏模式选择进行校验）
uchar check_gedree(uint choose_people, uint choose_difficulty);

// 交互操作函数声明
	// 交互界面
void  interaction();
void  interaction_1v1(uchar i);
	// 交互界面副函数（对每次取棋子数进行校验）
uchar check_number(uint getout, uint number);
uchar check_number_1v1(uint getout, uint number, uchar i);
	// 判断胜负函数
uint How_Win();
uint How_Lose();
	// 错误！！警告
void error(void);
	// 写文件
void writefile_num(unsigned char choose);
	// 计时
uchar IF_GameTime(uchar times, uchar choose);
	// 多线程
//void create_pthread(pthread_t thread1, int tmp1);
	// 线程函数
//unsigned char print_message_function(void *ptr);
// 机器算法函数声明
	// 机器算法
// 判断三组是否有任意两组相同
	// 参数：无
	// 返回值：若相等则返回不相等的堆位数，若不相等则返回0，
	// [在之后若要使用到这个选定的堆，则使用switch进行等值选择即可]
uchar IF_equal();
// 判断三组中是否有一组为1，此时没有两个为1的可能，因为之前判断过是否有任意两堆相同。
	// 参数：无
	// 返回值：若为1则返回为1的堆位数，没有为1的堆则返回0，
uchar IF_isone();
// 判断剩下两组是否相邻
	// 参数：无
	// 返回值：相邻则返回小的堆位数，不相邻则返回0
uchar IF_next1();
// 判断任意两组是否相邻
	// 参数：无
	// 返回值：若相临则返回不相临的堆位数，若不相临则返回0，
uchar IF_next2();
// 判断相邻两个数小的数是否为偶数
	// 参数：数组指针Namehear[]
	// 返回值：若为偶数返回1，否则返回2
uchar IF_evenness1();
// 判断小的数据是否为偶数
	// 参数：无
	// 返回值：若为偶数返回小的堆得堆位，否则返回0
uchar IF_evenness2();
// 判断是否大于30
	// 参数：无
	// 返回值：如果大于30则返回1，小于30则返回0
uchar IF_evenness3();
// 判断是否大于30
	// 参数：无
	// 返回值：如果大于30则返回1，小于30则返回0
uchar IF_GreaterThan30();
// 判断这个偶数是否大于10
	// 参数：无
	// 返回值：如果大于10则返回1，小于10则返回0
uchar IF_GreaterThan10();
// 机器算法
void robot();
// 找到最大的堆
unsigned int Get_Max();
// 找到中间的堆
unsigned int Get_Center();
// 找到最小的堆
unsigned int Get_Min();

#endif
