/****************************************  机器算法函数  *****************************************/
/*																								 */
/*	函数实现:																					 */
/*		1、机器算法主逻辑																		 */
/*		2、找到最大的堆																			 */
/*		3、找到中间的堆																			 */
/*		4、找到最小的堆																			 */
/*		5、判断是否有任意两堆相等																 */
/*		6、判断是否有任意两堆相临																 */
/*		7、判断三组是否有任意两组相同															 */
/*		8、判断三组中是否有一组为1																 */
/*		9、判断剩下两组是否相邻																	 */
/*	   10、判断任意两组是否相邻																	 */
/*	   11、判断相邻两个数小的数是否为偶数(剩下两组相邻)											 */
/*	   12、判断相邻两个数小的数是否为偶数(任意两组相邻)											 */
/*	   13、判断小的数据是否为偶数																 */
/*	   14、判断是否大于30																		 */
/*	   15、判断这个偶数是否大于10				 												 */
/*						 																		 */
/*************************************************************************************************/

/****************************************  头文件包含  ******************************************/
#include "qizi.h"

/*****************************************  参数定义  *******************************************/


/****************************************   函数原型   *****************************************/

// 机器算法
void robot() 
{
	printf("该机器取了！");
	// 是否有两个为0
	if (((Namehear[1] == 0) + (Namehear[2] == 0) + (Namehear[3] == 0)) == 2)	// 判断有几个为零 ，加起来就是几
	{
		// 有两个为0
		Namehear[(Namehear[1] == 0) ? 0 : 1] = 0;
		Namehear[(Namehear[2] == 0) ? 0 : 2] = 0;
		Namehear[(Namehear[3] == 0) ? 0 : 3] = 0;
	}
	else
	{
		// 是否有一个为0 // 测试正确
		if (((Namehear[1] == 0) + (Namehear[2] == 0) + (Namehear[3] == 0)) == 1)
		{
			// 判断是否有任意两组相等
			if (IF_equal())// 测试正确
			{
				// 判断这两堆是否为1
				if ((Namehear[1] + Namehear[2] + Namehear[3]) == 2)
				{
					Namehear[(Namehear[1] == Namehear[2]) ? 1 : 0] = 0;
					Namehear[(Namehear[2] == Namehear[3]) ? 2 : 0] = 0;
					Namehear[(Namehear[3] == Namehear[1]) ? 3 : 0] = 0;
				}
				else
				{
					// 任意1堆取为 1
					Namehear[(Namehear[1] == Namehear[2]) ? 1 : 0] = 1;
					Namehear[(Namehear[2] == Namehear[3]) ? 2 : 0] = 1;
					Namehear[(Namehear[3] == Namehear[1]) ? 3 : 0] = 1;
				}
			}
			else
			{
				// 不相等取为相等(让大的等于小的)。
				if (Namehear[1] == 0)
				{
					Namehear[2] = Namehear[3] = Namehear[(Namehear[2] > Namehear[3]) ? 3 : 2];
				}
				else if (Namehear[2] == 0)
				{
					Namehear[1] = Namehear[3] = Namehear[(Namehear[1] > Namehear[3]) ? 3 : 1];
				}
				else if (Namehear[3] == 0)
				{
					Namehear[2] = Namehear[1] = Namehear[(Namehear[2] > Namehear[1]) ? 1 : 2];
				}
			}
		}
		else
		{
			// 没有0 判断三组是否有任意两组相同
			if (IF_equal()) // 测试正确
			{
				// 相同
				Namehear[IF_equal()] = 0;	// 把不相等的取为0
			}
			else
			{
				/* 任意三个不相等 */
				// 判断三组中是否有一组为1
				if (IF_isone())
				{	
					// 判断剩下两组是否相邻
					if (IF_next1())
					{	
						// 判断相邻两个数小的数是否为偶数
						if (IF_evenness1())
						{	
							// 是偶数
							// 判断是否大于30
							if (IF_GreaterThan30())
							{
								Namehear[IF_evenness2()] -= 20;	// 若最小的大于30
							}
							else
							{	// 判断这个偶数是否大于10
								if (IF_GreaterThan10())
								{
									Namehear[IF_evenness2()] -= 8;
								}
								else
								{
									// 分情况讨论
									switch (Namehear[IF_evenness2()])
									{
									case 10:Namehear[IF_evenness2()] -= 4; break;
									case 8: Namehear[IF_evenness2()] -= 4; break;
									case 6: Namehear[IF_evenness2()] -= 2; break;
									case 4: Namehear[IF_evenness2()] -= 2; break;
									case 2: Namehear[IF_evenness2()] -= 1; break;
									default: return 0;
									}
								}
							}
						}
						else
						{
							// 判断相邻两个数小的数是奇数,则把最大的一堆取为-2
							Namehear[Get_Max()] -= 2;
						}
					}
					// 测试完成
					else
					{	
						// 不相邻，判断小的数据是奇数还是偶数
						if (IF_evenness2())
						{
							// 是偶数
							Namehear[Get_Max()] = Namehear[Get_Center()] + 1;
						}
						else
						{
							// 是奇数
							Namehear[Get_Max()] = Namehear[Get_Center()] - 1;
						}
					}
				}
				// 测试完成
				else
				{
					// 没有一组为1 的时候 判断任意两组是否相邻
					if (IF_next2())
					{
						// 判断相邻两个数小的数是否为偶数
						if (IF_evenness3())
						{
							// 是偶数，把不相邻的取为一
							Namehear[IF_next2()] = 1;
						}
						else
						{
							// 是偶数，把不相邻的取为一
							Namehear[Get_Min()] = 1;
						}
					}
					else
					{
						/* 任意三组不相等、不位1、不相邻 */
						Namehear[Get_Min()] = 1;
					}
				}
			}
		}
		Ahear = Namehear[1];
		Bhear = Namehear[2];
		Chear = Namehear[3];
	}
	sleep(1);
	IF_GameTime(30, 12);		// 记录开始时间
	writefile_num(12);			// 写入文件进行记录
}

// 判断三组是否有任意两组相同
	// 参数：无
	// 返回值：若相等则返回不相等的堆位数，若不相等则返回0，
	// [在之后若要使用到这个选定的堆，则使用switch进行等值选择即可]
uchar IF_equal() {
	if (Namehear[1] == Namehear[2]) {
		return 3;
	}
	else if (Namehear[1] == Namehear[3]) {
		return 2;
	}
	else if (Namehear[2] == Namehear[3]) {
		return 1;
	}
	else {
		return 0;
	}
}

// 判断三组中是否有一组为1，此时没有两个为1的可能，因为之前判断过是否有任意两堆相同。
	// 参数：无
	// 返回值：若为1则返回为1的堆位数，没有为1的堆则返回0，
uchar IF_isone()
{
	if (Namehear[1] == 1) {
		return 1;
	}
	else if (Namehear[2] == 1) {
		return 2;
	}
	else if (Namehear[3] == 1) {
		return 3;
	}
	else {
		return 0;
	}
}

// 判断剩下两组是否相邻
	// 参数：无
	// 返回值：相邻则返回小的堆位数，不相邻则返回0
uchar IF_next1() 
{
	// 这里是有1的情况，等值判断为1的组是那一组
	switch(IF_isone()) 
	{
		case 1: 
		{
			if ((Namehear[2] - Namehear[3]) == 1)		// 若为第一组 判断二三组那个小
			{
				return 3;
			}
			else if((Namehear[3] - Namehear[2]) == 1)
			{
				return 2;
			}
			else 
			{
				return 0;
			};break;
		}
		case 2: 
		{
			if ((Namehear[1] - Namehear[3]) == 1)		// 若为第二组 判断一三组那个小
			{
				return 3;
			}
			else if ((Namehear[3] - Namehear[1]) == 1)
			{
				return 1;
			}
			else
			{
				return 0;
			}; break;
		}
		case 3: 
		{
			if ((Namehear[1] - Namehear[2]) == 1)		// 若为第三组 判断一二组那个小
			{
				return 2;
			}
			else if ((Namehear[2] - Namehear[1]) == 1)
			{
				return 1;
			}
			else
			{
				return 0;
			}; break;
		}
		default :
			// 这里是没有1的情况
			return 0;		// 不可能
			
	}
}

// 判断任意两组是否相邻
	// 参数：无
	// 返回值：若相临则返回不相临的堆位数，若不相临则返回0，
uchar IF_next2() {
	if (((Namehear[1] - Namehear[2]) == 1) || ((Namehear[2] - Namehear[1]) == 1))
	{
		return 3;
	}
	else if (((Namehear[1] - Namehear[3]) == 1) || ((Namehear[3] - Namehear[1]) == 1))
	{
		return 2;
	}
	else if (((Namehear[3] - Namehear[2]) == 1) || ((Namehear[2] - Namehear[3]) == 1))
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}

// 判断相邻两个数小的数是否为偶数(剩下两组相邻)
	// 参数：数组指针Namehear[]
	// 返回值：若为偶数返回1，否则返回2 ，最小的数是 IF_next1() 的值
uchar IF_evenness1()
{
	if(!(Namehear[IF_next1()] % 2))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

// 判断相邻两个数小的数是否为偶数(任意两组相邻)
	// 参数：数组指针Namehear[]
	// 返回值：若为偶数返回1，否则返回2， 最小的数是 IF_next2() 的值
uchar IF_evenness3()
{
	if (!(Namehear[IF_next2()] % 2))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

// 有一堆为1，判断小的数据是否为偶数
	// 参数：无
	// 返回值：若为偶数返回小的堆得堆位，否则返回0
uchar IF_evenness2()
{
	switch (IF_isone())
	{
		case 1:
		{
			if (Namehear[2] > Namehear[3])			// 第1堆为1，则判断二三堆的大小
			{
				if (!(Namehear[3] % 2))
				{
					return 3;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if (!(Namehear[2] % 2))
				{
					return 2;
				}
				else
				{
					return 0;
				}
			}; break;
		}
		case 2:
		{
			if (Namehear[1] > Namehear[3])		// 第2堆为1，则判断二三堆的大小
			{
				if (!(Namehear[3] % 2))
				{
					return 3;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if (!(Namehear[1] % 2))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}; break;
		}
		case 3:
		{
			if (Namehear[1] > Namehear[2])			// 第3堆为1，则判断二三堆的大小
			{
				if (!(Namehear[2] % 2))
				{
					return 2;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				if (!(Namehear[1] % 2))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}; break;
		}
		default :
			return 0;	// 不可能
	}
}

// 判断是否大于30
	// 参数：无
	// 返回值：如果大于30则返回1，小于30则返回0
uchar IF_GreaterThan30()
{
	if (Namehear[IF_evenness2()] > 30)
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}

// 判断这个偶数是否大于10
	// 参数：无
	// 返回值：如果大于10则返回1，小于10则返回0，最小的数是 IF_next1() 的值
uchar IF_GreaterThan10()
{
	if (Namehear[IF_evenness2()] > 10)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

// 找到最大的堆
uint Get_Max() {

	if (Namehear[1] >= Namehear[2])
		if (Namehear[1] >= Namehear[3]) {
			return 1;
		}
		else {
			return 3;
		}
	else {
		if (Namehear[2] >= Namehear[3]) {
			return 2;
		}
		else {
			return 3;
		}
	}
}

// 找到中间的堆
uint Get_Center() {

	if (Namehear[1] >= Namehear[2])
		if (Namehear[1] >= Namehear[3]) {
			return 3;
		}
		else {
			return 1;
		}
	else {
		if (Namehear[2] >= Namehear[3]) {
			return 3;
		}
		else {
			return 2;
		}
	}

}

// 找到最小的堆
uint Get_Min() {

	if (Namehear[1] <= Namehear[2])
		if (Namehear[1] <= Namehear[3]) {
			return 1;
		}
		else {
			return 3;
		}
	else {
		if (Namehear[2] <= Namehear[3]) {
			return 2;
		}
		else {
			return 3;
		}
	}
}


/* 牛逼的想法
// 判断是否有任意两组相等
			if (IF_equal())
			{
				// 判断这两堆是否为1
				if (Namehear[1] + Namehear[2] + Namehear[3])
				{
					Namehear[(Namehear[1] == Namehear[2]) ? 1 : 0] = 0;
					Namehear[(Namehear[2] == Namehear[3]) ? 2 : 0] = 0;
					Namehear[(Namehear[3] == Namehear[1]) ? 3 : 0] = 0;
				}
				else
				{
					Namehear[(Namehear[1] == Namehear[2]) ? 1 : 0] = 1;
					Namehear[(Namehear[2] == Namehear[3]) ? 2 : 0] = 1;
					Namehear[(Namehear[3] == Namehear[1]) ? 3 : 0] = 1;
				}
			}
*/



